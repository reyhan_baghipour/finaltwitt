import React from 'react';
import './Layout.css';
import LeftSideBar from '../Component/LeftSideBar/LeftSideBar';
import RightSideBar from '../Component/RightSideBar/RightSideBar';
import Divaider from '../Component/Divaider/Divaider';




const Layout = (props) => {
    return ( 
    <div className = "Root">
        <RightSideBar/>
        <Divaider/>
        <div className = "MainRoot">  
            {props.children}        
        </div> 
        <Divaider/>
        <LeftSideBar/>
    </div>
    );
};

export default Layout;
import React from 'react';
import Layout from './Layout/Layout';
import {Switch,BrowserRouter,Route,Redirect} from 'react-router-dom';
import Home from './Pages/Home/Home';
import TwitterUsers from './Pages/UserTwitter/TwitterUsers';
import MainUser from './Pages/MainUser/MainUser';
import Login from './Pages/Login/Login';
import {ToastContainer} from 'react-toastify';
import HashtagsPage from './Pages/HashtagsPage/HashtagsPages';
import Page404 from './Pages/Page404/Page404';


function App() {
    return ( 
        <BrowserRouter>
        <Switch>
            <PublicRoute path={"/Login"} component={Login}/>
            <PrivateRoute path={"/"} render={()=>
                <Layout>
                    <Switch>
                        <Route exact path={"/"} component={Home}/>
                        <Route exact path={"/TwittUsers/:User"} component={TwitterUsers}/>
                        <Route exact path={"/Hashtags/:Hash"} component={HashtagsPage}/>
                        <Route exact path={"/MainUser"} component={MainUser}/>
                        <Route exact component={Page404}/>
                    </Switch>
                </Layout>
            }/>
            </Switch>
            <ToastContainer/>
        </BrowserRouter>
    );
}

const IsLogin=()=>!!localStorage.getItem("Id");
const PublicRoute=({component,...props})=>{
    return <Route {...props} render={(props)=>{
        if(IsLogin()) 
            return <Redirect to={"/"}/>
        else 
            return React.createElement(component,props);
    }}/>
};
const PrivateRoute=({render,...props})=>{
    return <Route {...props} render={(props)=>{
        if(IsLogin()) 
            return render(props);
        else 
            return <Redirect to={"/Login"}/>
    }}/>
};
export default App;
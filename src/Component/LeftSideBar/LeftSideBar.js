import React,{useEffect, useState,useRef} from 'react';
import './LeftSidebar.css';
import {Menu,MenuItem} from '@szhsin/react-menu';
import '@szhsin/react-menu/dist/index.css';
import {GetAllReporters} from '../../Api/ApiTwitt';
import {Link} from 'react-router-dom';


const LeftSideBar = () => {
    const [Reporters,setReporters]=useState([]);
    const [Image,setImage]=useState();
    const [ImagePath,setImagePath]=useState();
    const InputRef = useRef();

    useEffect(()=>{
        GetAllReporters((isok,data)=>{
            if(!isok)
                return alert(data.message);
            else setReporters(data);
        });
    },[]);
    const Exit=()=>{
        localStorage.clear();
        window.location.reload();
    };
    const LoadImage=()=>{
       InputRef.current.click();
    };
    const HandelImageProfile=(e)=>{
        if (e.target.files && e.target.files.length>0){
            setImage(e.target.files[0]);
            const reader=new FileReader();
            reader.onload=(e)=>{
                setImagePath(e.target.result);
            }
            reader.readAsDataURL(e.target.files[0]);
        }
    };
    const SetImage=()=>{
        if (ImagePath)
            return ImagePath;
        return "Images/UserImage.jpg";
    };
    return ( 
        <div className="LeftSidebarBody">
            <div className="UserInformation">
                <input ref={InputRef} type={"file"} style={{display:"none"}} onChange={HandelImageProfile}/>
                <Link to={"/MainUser"}>
                    <div className="NameAndId">
                        <span className="Name">{localStorage.getItem("Name")}</span>
                        <span className="Id">{localStorage.getItem("UserName")}</span>
                    </div>
                </Link>
                <Menu menuButton={
                    <div className="UserImage">
                        <img src={SetImage()} alt="UserImage"/>
                    </div>
                }>
                    <MenuItem className="ItemMenu" onClick={LoadImage}>آپلود عکس</MenuItem>
                    <MenuItem className="ItemMenu">تنضیمات</MenuItem>
                    <MenuItem className="ItemMenu" onClick={Exit}>خروج</MenuItem>
                </Menu>
            </div>
            <div className="Reporters">
                <span className="ReportersTitr">دوستان من</span>
                {
                    Reporters.map(item=>
                        <Link to={`/TwittUsers/${item.NameInfo}`}>
                            <button className="ReportersList">
                                <div className="ReportersImage">
                                    <img src={item.ProfileImageInfo} alt="UserImage"/>
                                </div>
                                <div className="RNameAndId">
                                    <span className="RName">{item.NameInfo}</span>
                                    <span className="RId">{item.UserNameInfo}</span>
                                </div>
                            </button>
                        </Link>
                    )
                }
            </div>
        </div>
    );
};



export default LeftSideBar;
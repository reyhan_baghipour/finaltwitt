import React , {useState , useEffect} from 'react';
import './RightSideBar.css';
import {GetAllHashtags} from '../../Api/ApiTwitt';
import {Link} from 'react-router-dom';

const RightSideBarBody = (props) => {
    const [Hashtags,setHashtags]=useState([]);
    useEffect(()=>{
        GetAllHashtags((isok,data)=>{
            if(!isok)
                return alert(data.message);
            else setHashtags(data);
        });
    },[]);
    return (
        <div className="RightSideBarBody">
            <Link to={"/"}>
                <div className="LogoTwitter">
                    <img className="Logo" src="./Images/LogoTwitter.png" alt="TwitterLogo"/>
                    <div className="Titr">توییتر فارسی</div>
                </div>
            </Link>
            <div className="HashtagsList">
                <div className="HashtagsTitr">داغ ترین هشتگ ها</div>
                {
                    Hashtags.map(item=>
                        <Link to={`/Hashtags/${item.Titr}`}>
                            <button className="List" key={item.id}>
                                <img className="HashtagsImage" src={item.Image} alt="Hashtag" />
                                <span className="HashtagsText">{item.Titr}</span>
                            </button>
                        </Link>
                    )
                }
            </div>
        </div>       
    );
};



export default RightSideBarBody;
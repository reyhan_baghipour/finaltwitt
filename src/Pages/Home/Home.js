import React ,{useState,useEffect} from 'react';
import Header from '../Component/Header/Header';
import NewTwitt from '../Component/NewTwitt/NewTwitt';
import { FcHome } from 'react-icons/fc';
import {GetAllTwitt} from '../../Api/ApiTwitt';
import TwittList from '../Component/TwittList/TwittList';


const Home = ()=>{
    const [ListTwitt,setListTwitt]=useState([]);
        useEffect(()=>{
            GetAllTwitt((isok,data)=>{
                if(!isok)
                    return alert(data.message);
                else setListTwitt(data);
            });
    },[]);
    return(
        <div className="HomeRoot">
            <Header title={"خانه"} icon={<FcHome/>}/>
            <NewTwitt/>
            {
                ListTwitt.map(item=> <TwittList key={item.id} Name={item.Name} User={item.IdUser} Text={item.Text} Image={item.Image} Like={item.Like}/>)
            }
        </div>
    );
};


export default Home;
import React from 'react';
import './Page404.css';


const Page404 = () => {
    return (
        <div className="Page404Root">
            <div className="Body404">
                <img src="/Images/LogoTwitter.png" alt="TwitterLogo"/>
                <h1>404</h1>
            </div>
        </div>
    );
};

export default Page404;
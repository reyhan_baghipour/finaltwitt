import React , {useState} from 'react';
import { STabs, STabList, STab, STabPanel } from './StyleLogin';
import './StyleLogin';
import './Login.css';
import {GetUserNameAndPassword,GetUserRegister,PostRegisterInfo} from '../../Api/ApiLogin';
import {toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Login =()=>{
    //Login
    const [LoginUserName,setLoginUserName]=useState("");
    const [LoginPassword,setLoginPassword]=useState("");
    //Register
    const [RegisterName,setRegisterName]=useState("");
    const [RegisterUserName,setRegisterUserName]=useState("");
    const [RegisterPassword,setRegisterPassword]=useState("");
    const [ConfigRegisterPassword,setConfigRegisterPassword]=useState("");
    const HandelLogin=()=>{
        const User={
            username:LoginUserName,
            password:LoginPassword
        };
        const ValidateLogin=(User)=>{
            if(!User.username)
                return toast('لطفا نام کاربری خود را وارد نمایید.');
            if(!User.password)
                return toast('لطفا رمز عبور خود را وارد نمایید.');
        };
        const Error=ValidateLogin(User);
        if (Error)
            return Error;
        if((User.username)&&(User.password)){
            GetUserNameAndPassword((isok,data)=>{
                if(!isok){
                    return alert(data.message);
                }else{
                    const Info = data.find(item=>(item.UserName === User.username)&&(item.Password === User.password))
                    if (!Info)
                            return toast.error('نام کاربری و رمز عبور همخوانی ندارند.'); 
                        toast.success('شما با موفقیت وارد شدید.');
                        localStorage.setItem("Id",Info.id);
                        localStorage.setItem("Name",Info.Name);
                        localStorage.setItem("UserName",Info.UserName);
                        localStorage.setItem("Image",Info.Image);
                        window.location.reload();
                }
            });
        }
    };
    const handleRegisterSubmit=(event)=>{
        event.preventDefault();
        const RegisterInfo={
            id:Math.floor(Math.random()*1000),
            Name:RegisterName,
            UserName:RegisterUserName,
            Password:RegisterPassword,
            Image:""
        };
        const Config={
            configpass:ConfigRegisterPassword,
        }
        if (!RegisterName)
            return toast.info('نام خود را وارد نمایید.');
        if (!RegisterUserName)
            return toast.info('نام کاربری خود را وارد نمایید.');
        if (!RegisterPassword)
            return toast.info('رمز عبور خود را وارد نمایید.');
        if (Config.configpass !== RegisterInfo.Password)  
            return toast.info('رمز عبور و تکرار رمز همخوانی ندارند.');
        GetUserRegister((isok,data)=>{
            if(!isok){
                return alert(data.message);
            }else if (data.length>0){
                const Result=data.find(Main=> {return Main.UserName === RegisterUserName});
                 if (Result){
                    return toast.error('این نام کاربری قبلاً ثبت شده است');
                }else{
                    PostInfo();
                }   
            };
        });
        const PostInfo=()=>{
            PostRegisterInfo(RegisterInfo,(isok)=>{
                if(!isok)
                    return toast.error("ثبت نام انجام نشد.");
                toast.success("ثبت نام با موفقیت انجام گردید.");
                localStorage.setItem("Id",RegisterInfo.id);
                localStorage.setItem("Name",RegisterInfo.Name);
                localStorage.setItem("UserName",RegisterInfo.UserName);
                localStorage.setItem("Image",RegisterInfo.Image);
            });
        };
    };
    return(
        <div className="LoginRoot">
            <div className="LoginBody">
            <STabs selectedTabClassName='is-selected' selectedTabPanelClassName='is-selected'>
                <STabList>
                    <STab>ورود</STab>
                    <STab>ثبت نام</STab>
                </STabList>
                <STabPanel>
                    <div className="LoginForm">
                        <label htmlFor="UserName">نام کاربری:</label>
                        <input type="text" id="UserName" name="UserName" value={LoginUserName} onChange={e=>{setLoginUserName(e.target.value)}}/>
                        <label htmlFor="Password">رمز عبور:</label>
                        <input type="Password" id="Password" name="Password" value={LoginPassword} onChange={e=>setLoginPassword(e.target.value)}/>
                        <div className="Button">
                            <button className="Login" onClick={HandelLogin}>ورود</button>
                        </div>
                    </div>
                </STabPanel>
                <STabPanel>
                    <form className="RegisterForm" onSubmit={handleRegisterSubmit}>
                        <label htmlFor="FullName">نام:</label>
                        <input type="text" id="FullName" name="UserName" value={RegisterName} onChange={e=>{setRegisterName(e.target.value)}}/>
                        <label htmlFor="UserName">نام کاربری:</label>
                        <input type="text" id="UserName" name="UserName" value={RegisterUserName} onChange={e=>{setRegisterUserName(e.target.value)}}/>
                        <label htmlFor="Password">رمز عبور:</label>
                        <input type="Password" id="Password" name="Password" value={RegisterPassword} onChange={e=>{setRegisterPassword(e.target.value)}}/>
                        <label htmlFor="ConfigPassword">تکرار رمز عبور:</label>
                        <input type="Password" id="ConfigPassword" name="ConfigPassword" value={ConfigRegisterPassword} onChange={e=>{setConfigRegisterPassword(e.target.value)}}/>
                        <div className="Button">
                            <button className="Register" type="submit">ثبت نام</button>
                        </div>
                    </form>
                </STabPanel>
            </STabs>
            </div>
        </div>
    );
};


export default Login;
import Styled from 'styled-components';
import { Tabs, TabList, Tab, TabPanel } from 'react-tabs';


export const STabs = Styled(Tabs)
`
  width: 100%;
  direction:rtl;
`;
STabs.tabsRole = 'Tabs';

export const STabList = Styled(TabList)
`
  list-style-type: none;
  padding: 0 0 0.25rem 0;
  display: flex;
  margin: 0;
`;
STabList.tabsRole = 'TabList';

export const STab = Styled(Tab)
`
  border-bottom: 0.15rem solid RGB(29, 161, 242);
  padding: 1rem 2rem 1rem 2rem;
  user-select: none;
  cursor: arrow;
  width:50%;
  text-align:center;
  font-size:1.25rem;
  font-weight:bold;
  

  &.is-selected {
    // color: white;
    // background: black;
    border-bottom: 1px solid white;
  }

  &:focus {
    outline: none;
  }
`;
STab.tabsRole = 'Tab';

export const STabPanel = Styled(TabPanel)
`
  display: none;
  margin-top: -5px;


  &.is-selected {
    display: block;
  }
`;
STabPanel.tabsRole = 'TabPanel';


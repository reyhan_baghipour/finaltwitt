import React,{useState,useEffect} from 'react';
import Header from '../Component/Header/Header';
import {GetAllUserInformation,GetAllUserTwitts} from '../../Api/ApiTwitt';
import TwittList from '../Component/TwittList/TwittList';
import UserProfilePage from '../Component/UserProfilePage/UserProfilePage';

const MainUser=()=>{
    const [UserInformation,setUserInformation]=useState([]);
    const [UserTwitts,setUserTwitts]=useState([]);
    useEffect(()=>{
        GetAllUserInformation((isok,data)=>{
            if(!isok)
                return alert(data.message);
            else setUserInformation(data);
        });
    },[]);
    useEffect(()=>{
        GetAllUserTwitts((isok,data)=>{
            if(!isok)
                return alert(data.message);
            else setUserTwitts(data);
        });
    },[]);
    return(
        <div>
            <Header title = {"ریحان"}/>  
            {
                UserInformation.map(item=><UserProfilePage Name={localStorage.getItem("Name")} UserName={localStorage.getItem("UserName")}
                CoverImage={item.CoverImage} ProfileImage={item.ProfileImage} Date={item.Date}/>)
            }
            {
                UserTwitts.map(item=><TwittList Name={localStorage.getItem("Name")} User={localStorage.getItem("UserName")}
                Image={item.Image} Text={item.Text} Like={item.Like} />)
            }
        </div>
    );
};

export default MainUser;
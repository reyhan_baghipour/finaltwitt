import React,{useState,useEffect} from 'react';
import Header from '../Component/Header/Header';
import { BiHash } from "react-icons/bi";
import {GetAllHashtags} from '../../Api/ApiTwitt';
import TwittList from '../Component/TwittList/TwittList';

const HashtagsPage=(props)=>{
    const [HashtagsTwitt,setHashtagsTwitt]=useState({});
    useEffect(()=>{
        GetAllHashtags((isok, data) => {
            if (!isok) return alert(data.message);
            else {
              const hashtag = data.find(
                (item) => item.Titr === props.match.params.Hash
              );
              if (hashtag) {
                setHashtagsTwitt(hashtag);
                console.log(hashtag);
              }
            }
        });
    },[props.match.params.Hash]);
    return(
        <div>
            <Header title = {props.match.params.Hash} icon = {<BiHash/>}/>  
            { HashtagsTwitt.Image?
                 <TwittList 
                  Name={HashtagsTwitt.TwitterName} 
                  User={HashtagsTwitt.UserName}
                  Text={HashtagsTwitt.TextHash} 
                  Image={HashtagsTwitt.ProfileImage} 
                  Like={HashtagsTwitt.Like}
                />
            :null}

        </div>
    );
};


export default HashtagsPage;
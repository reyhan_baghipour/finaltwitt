import React,{useEffect, useState} from 'react';
import Header from '../Component/Header/Header';
import { GoVerified } from "react-icons/go";
import TwittPageUsers from './TwittPageUsers';
import {GetAllReporters} from '../../Api/ApiTwitt';


const TwitterUsers = (props) => {
    const NameInfo = props.match.params.User;
    const [CurrentUser,setCurrentUser]=useState({});
    useEffect(()=>{
        GetAllReporters((isok, data) => {
            if (!isok)
                return alert(data.message);
            else {
                for(let user of data) {
                    if(user.NameInfo === NameInfo) {
                        setCurrentUser(user)
                    }
                }
            };
        });
    });
    return ( 
        <div className = "UserTwitterRoot">
            <Header title = { props.match.params.User } icon = {<GoVerified/>}/>  
            <TwittPageUsers 
                name={props.match.params.User}
                username={CurrentUser.UserNameInfo}
                profileimage={CurrentUser.ProfileImageInfo}
                coverimage={CurrentUser.CoverImageInfo}
                date={CurrentUser.DateInfo}
            />
        </div>
    );
};

export default TwitterUsers;
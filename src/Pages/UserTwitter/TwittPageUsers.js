import React from 'react';
import './TwittPageUsers.css';
import UserProfilePage from '../Component/UserProfilePage/UserProfilePage';

const TwittPageUsers=({name,username,coverimage,profileimage,date})=>{
    return(
        <div>
            <UserProfilePage Name={name} UserName={username}
                CoverImage={coverimage} ProfileImage={profileimage} Date={date}/>
        </div>
    );
};


export default TwittPageUsers;
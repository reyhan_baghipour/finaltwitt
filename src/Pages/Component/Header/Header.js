import React from 'react';
import './Header.css';

const Header = ({title,icon,iconback}) => {
    return (
        <div className="RootHeader">
            <span className="Icon">{icon}</span>
            <span className="TitrHeader">{title}</span>
        </div>
    );
};



export default Header;
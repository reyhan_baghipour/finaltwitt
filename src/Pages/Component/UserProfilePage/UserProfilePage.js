import React from 'react';
import { BsCalendar } from "react-icons/bs";
import { Link } from 'react-router-dom';
import './UserProfilePage.css';

const UserProfilePage = (props) => {
    return ( 
        <div className = "UserRoot" >
            <div className = "UserData" >
                < div className = "Images" >
                     <div className = "CoverImage" >
                        <img src = { props.CoverImage } alt = "CoverImage"/ >
                    </div> 
                    <div className = "ProfileImage" >
                         <img src = { props.ProfileImage } alt = "ProfileImage"/ >
                    </div> 
                     <button className = "Edit-Profile" > ویرایش پروفایل </button>
                 </div>
                < div className = "UserName-Id" >
                        <span className = "User-Name" > { props.Name } </span> 
                        < span className = "Id-Name" > { props.UserName } </span> 
                 </div>
                < div className = "StartDate" >
                <BsCalendar />
                <span className = "Date" >{props.Date}</span>
            </div>
            <div className = "Follower" >
                <Link to = { "/" }>
                <span className = "Date" > دنبال شونده 0 </span>
                </Link> 
                < Link to = { "/" }>
                <span className = "Date" > دنبال کننده 0 </span>
                </Link> 
            </div>
            </div>
            <div className = "Posts" >
                < ul className = "PostTitr" >
                    <li className = "TitrItem" > پست های توییت شده </li> 
                </ul> 
            </div> 
        </div>
    );
};

export default UserProfilePage;
import React from 'react';
import './TwittList.css';
import { BsFillHeartFill } from "react-icons/bs";
import { BiRepost } from "react-icons/bi";


const TwittList =(props)=>{
    const replacetext=(textPage)=>{
        return {__html:textPage.replace(/#\S+/g ,"<a href='#' style='color:RGB(29, 161, 242); text-decoration:none;'>$&</a>")}
    };
    return (
        <div className="RootTwittList">
             <div className="TwittList">
                 <div className="ImageNameId">
                    <div className="Imagetwitt">
                        <img src={props.Image} alt="ImageTwitt"/>
                    </div>
                    <span className="NameTwitt">{props.Name}</span>
                    <span className="IdTwitt">{props.User}</span>
                </div>
                <div className="TextTwitt" dangerouslySetInnerHTML={replacetext(props.Text)}></div>
                    <div className="TwittListButton">
                        <button className="ReTwitt">
                            <span className="IconRepost"><BiRepost/></span>
                        </button>
                        <button className="LikeTwitt">
                            <span className="IconHeart"><BsFillHeartFill/></span>
                        </button>
                        <span className="Likecounter">{props.Like}</span>
                </div>
            </div>
         </div>
    );
};

export default TwittList;
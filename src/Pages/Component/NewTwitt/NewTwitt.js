import React from 'react';
import './NewTwitt.css';
import { BsFillImageFill } from "react-icons/bs";
import {NotificationManager} from 'react-notifications';
import {NewTwittRequest} from '../../../Api/ApiTwitt';
import {TwittRequest} from '../../../Api/ApiTwitt';

const NewTwitt =()=>{
    const TextInput=React.createRef();
    const onChange=()=>{
        const text=TextInput.current.value;
        console.log(text);
    };
    const onClick=()=>{
        const Twitttext=TextInput.current.value;
        const data={
            id:Math.floor(Math.random()*1000),
            "Image": "/Images/UserImage.jpg",
            "Name": "ریحانه باقی پور طرقی",
            "IdUser": "@reyhan-bghipour",
            "Text": Twitttext,
            "Like": 50
        };
        NewTwittRequest(data,(isok)=>{
            if(!isok)
                return NotificationManager.error("توییت شما ارسال نشد");
            else return NotificationManager.success("توییت شما ارسال شد");
        });
        const UserTwittstext=TextInput.current.value;
        const Twitt={
            id:Math.floor(Math.random()*1000),
            "Image": "/Images/UserImage.jpg",
            "Name": "ریحانه باقی پور طرقی",
            "IdUser": "@reyhan-bghipour",
            "Text": UserTwittstext,
            "Like": 50
        };
        TwittRequest(Twitt,(isok)=>{
            if(!isok)
                return NotificationManager.error("توییت شما ارسال نشد");
            else return NotificationManager.success("توییت شما ارسال شد");
        });
    };
    return(
        <div className="RootNewTwitt">
            <div className="textNewTwitt">
                <div className="ImageNewTwitt">
                        <img src="Images/UserImage.jpg" alt="UserImage"/>
                </div>
                <textarea className="NewTwitt" onChange={onChange} ref={TextInput} name="NewTwitt" id="NewTwitt" rows="6" placeholder="توییت کن ..."></textarea>
            </div>
            <div className="AllButton">
                <button className="LoadingImage">
                    <span className="LoadIcon">
                        <BsFillImageFill/>
                    </span>
                </button>
                <button className="Twitt" onClick={onClick}>توییت</button>
            </div>
        </div>
    );
};

export default NewTwitt;
import axios from 'axios';

export const GetAxiosInstance = () => {
    return axios.create({
        baseURL: "http://localhost:3000",
    });
};
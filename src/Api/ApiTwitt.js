import { GetAxiosInstance } from './Api';

export const GetAllHashtags = (callback) => {
    GetAxiosInstance().get("/HashtagsLists")
        .then(response => {
            const data = response.data;
            callback(true, data);
        }).catch(error => {
            callback(false, error);
        })
};

export const GetAllReporters = (callback) => {
    GetAxiosInstance().get("/ReportersList")
        .then(response => {
            const data = response.data;
            callback(true, data);
        }).catch(error => {
            callback(false, error);
        })
};
export const GetAllTwitt = (callback) => {
    GetAxiosInstance().get("/TwittList")
        .then(response => {
            const data = response.data;
            callback(true, data);
        }).catch(error => {
            callback(false, error);
        })
};

export const NewTwittRequest = (data, callback) => {
    GetAxiosInstance().post("/TwittList", data)
        .then(response => {
            callback(true);
        }).catch(error => {
            callback(false);
        })
};
export const TwittRequest = (data, callback) => {
    GetAxiosInstance().post("/UserTwitts", data)
        .then(response => {
            callback(true);
        }).catch(error => {
            callback(false);
        })
};
export const GetAllUserInformation = (callback) => {
    GetAxiosInstance().get("/UserInformation")
        .then(response => {
            const data = response.data;
            callback(true, data);
        }).catch(error => {
            callback(false, error);
        })
};
export const GetAllUserTwitts = (callback) => {
    GetAxiosInstance().get("/UserTwitts")
        .then(response => {
            const data = response.data;
            callback(true, data);
        }).catch(error => {
            callback(false, error);
        })
};
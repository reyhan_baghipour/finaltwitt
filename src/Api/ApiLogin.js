import { GetAxiosInstance } from './Api';

export const PostRegisterInfo = (RegisterInfo, callback) => {
    GetAxiosInstance().post("/Register", RegisterInfo)
        .then(response => {
            const data = response.data;
            callback(true, data);
        }).catch(error => {
            callback(false, error.response.data.massage);
        })
};

export const GetUserNameAndPassword = (callback) => {
    GetAxiosInstance().get("/Register")
        .then(response => {
            const data = response.data;
            callback(true, data);
        }).catch(error => {
            callback(false, error.response.data.massage);
        })
};

export const GetUserRegister = (callback) => {
    GetAxiosInstance().get("/Register")
        .then(response => {
            const data = response.data;
            callback(true, data);
        }).catch(error => {
            callback(false, error.response.data.massage);
        })
};
export const UploadImage = (Photo, callback) => {
    GetAxiosInstance().post("/UploadImage", Photo)
        .then(response => {
            const data = response.data;
            callback(true, data);
        }).catch(error => {
            callback(false, error.response.data.massage);
        })
};